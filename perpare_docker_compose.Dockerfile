## GitLab CI 準備docker-compose檔案需要一些Linux指令套件
## 由於頻繁存取會導致被拒絕連線，因此將其包於預設Image之內

FROM ubuntu:20.10

ENV DEBIAN_FRONTEND noninteractive
ENV CHILD_SCRIPT_DOWNLOAD_URL https://gitlab.gpu.mlc.app/csie-406/doc-gitlab-406/-/raw/master/templates/child/child-docker-compose-up-build.yml?inline=false

RUN mkdir -p .apt-cache && \
    apt-get -o dir::cache::archives=${PWD}/.apt-cache update && \
    apt-get -o dir::cache::archives=${PWD}/.apt-cache install -y gettext curl

# perpare_docker_compose:
#   stage: .pre
#   image:
#     name: ubuntu:20.10
#     entrypoint: [""]
#   variables:
#     CHILD_SCRIPT_DOWNLOAD_URL: 'https://gitlab.gpu.mlc.app/csie-406/doc-gitlab-406/-/raw/master/templates/child/child-docker-compose-up-build.yml?inline=false'
#     DEBIAN_FRONTEND: noninteractive
#   rules:
#     - if: $DOCKER_IP
#       exists:
#       - docker-compose.yml
#   before_script:
#     - mkdir -p .apt-cache
#     - apt-get -o dir::cache::archives=${PWD}/.apt-cache update
#     - apt-get -o dir::cache::archives=${PWD}/.apt-cache install -y gettext curl


